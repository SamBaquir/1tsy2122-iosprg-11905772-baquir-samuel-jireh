﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public string[] swipeDirections = { "Left", "Down", "Up", "Right" };
    public string assignedDirection;
    private float moveSpeed = 3.5f;
    public Rigidbody rb;

    public SpriteRenderer spriteRenderer;
    public Color newColor;

    private void Awake()
    {
    }
    protected void Update()
    {
        if (this.transform.position.y <= -11.53)
        {
            Destroy(this.gameObject);
        }
    }

    protected void GetRigidBody()
    {
        rb = this.GetComponent<Rigidbody>();
    }

    protected void Movement()
    {
        rb.velocity = new Vector3(rb.velocity.x, -1f, rb.velocity.z) * moveSpeed;
    }

    protected void AssignDirection()
    {

        if (assignedDirection == "Left")
        {
            gameObject.tag = "LeftSwipe";
        }

        else if (assignedDirection == "Right")
        {
            gameObject.tag = "RightSwipe";
        }

        else if (assignedDirection == "Down")
        {
            gameObject.tag = "DownSwipe";
        }

        else if (assignedDirection == "Up")
        {
            gameObject.tag = "UpSwipe";
        }

        //Debug.Log(gameObject.tag);
    }

    public void DestroyItself()
    {
        Destroy(this.gameObject);
    }

    public virtual void ExposeItself()
    {
        GameObject arrow = transform.GetChild(0).gameObject;
        spriteRenderer = arrow.GetComponent<SpriteRenderer>();
    }

}
