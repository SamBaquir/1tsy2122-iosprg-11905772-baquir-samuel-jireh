﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DashBar : MonoBehaviour
{
    public GameObject dashButton;

    public Slider slider;

    public void SetMaxEnergy(int energy)
    {
        slider.maxValue = energy;
        slider.value = 0;
    }

    public void SetDashEnergy(int energy)
    {
        slider.value = energy;
    }

    public void enableDashButton()
    {
        if (slider.value == slider.maxValue)
        {
            dashButton.gameObject.SetActive(true);
        }
    }
    public void disableDashButton()
    {
       
        dashButton.gameObject.SetActive(false);
        
    }
}
