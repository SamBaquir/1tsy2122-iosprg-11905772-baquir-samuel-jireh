﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    public bool powerUpActivated;

    public GameObject bossEnemy;
    public GameObject mainCamera;
    public GameObject dashButton;
    public GameObject bossSpawner;

    public int requiredObstaclesDestroyed = 15;
    public int currentObstaclesDestroyed;
    
    public DashBar dashBar;

    public Text outputText;

    public int life = 1;
    public int score = 0;

    private Vector2 startTouchPosition;
    private Vector2 currentPosition;
    private Vector2 endTouchPosition;


    public bool dashed = false;
    private bool stopTouch = false;
    bool bossSpawned = false;

    public float swipeRange;
    public float tapRange;

    public string playerInput;

    public GameObject targetEnemy;

    // Start is called before the first frame update
    void Start()
    {
        powerUpActivated = false;
        currentObstaclesDestroyed = 0;
        dashBar.SetMaxEnergy(requiredObstaclesDestroyed);
    }

    // Update is called once per frame
    void Update()
    {
        if (!bossSpawned && bossEnemy != null)
        {
            bossEnemy = null;
        }

        if (!powerUpActivated)
        {
            Dash();
        }

        if (powerUpActivated)
        {
            PowerUpActive();
        }

    }

    public void BossSpawned()
    {
        Debug.Log("FindBoss");
        bossEnemy = GameObject.Find("BossEnemy(Clone)");
        bossSpawned = true;
    }
    
    public void BossDead()
    {
        bossEnemy = null;
        bossSpawned = false;
    }

    public void Dash()
    {
        dashed = false;
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            startTouchPosition = Input.GetTouch(0).position;
        }
        
       if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
       {
           currentPosition = Input.GetTouch(0).position;
           Vector2 Distance = currentPosition - startTouchPosition;

       }
       
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            stopTouch = false;

            endTouchPosition = Input.GetTouch(0).position;

            Vector2 Distance = endTouchPosition - startTouchPosition;

            if (Mathf.Abs(Distance.x) < tapRange && Mathf.Abs(Distance.y) < tapRange)
            {
                
                this.transform.position = new Vector2(transform.position.x, transform.position.y + 1f);
                playerInput = "Tap";
                outputText.text = playerInput;
                score++;
                dashed = true;
            }

        }
    }


    void OnTriggerEnter(Collider collider)
    {
        playerInput = null;

        if (collider.gameObject.GetComponent<Enemy>() && targetEnemy == null)
        {
            targetEnemy = collider.gameObject;
            collider.gameObject.GetComponent<Enemy>().ExposeItself();
        }

    }

    void OnTriggerStay(Collider collider)
    {
        if (powerUpActivated && collider.gameObject.GetComponent<Enemy>())
        {
            targetEnemy = collider.gameObject;
            collider.gameObject.GetComponent<Enemy>().DestroyItself();
            targetEnemy = null;
            score += 5;
            
        }

        if (collider.gameObject.GetComponent<Enemy>() && targetEnemy == null)
        {
            targetEnemy = collider.gameObject;
            collider.gameObject.GetComponent<Enemy>().ExposeItself();
        }

        if (!powerUpActivated && collider.gameObject == targetEnemy)
        {
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                
                startTouchPosition = Input.GetTouch(0).position;
            }

            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                currentPosition = Input.GetTouch(0).position;
                Vector2 Distance = currentPosition - startTouchPosition;

                if (!stopTouch)
                {
                
                    if (targetEnemy.tag == "LeftSwipe" && Distance.x < -swipeRange)
                    {

                        Debug.Log("LeftSwipe Match");
                        this.transform.position = new Vector2(transform.position.x, transform.position.y + 1);
                        collider.gameObject.GetComponent<Enemy>().DestroyItself();
                        targetEnemy = null;
                        playerInput = "LeftSwipe";
                        outputText.text = playerInput;
                        stopTouch = true;
                        currentObstaclesDestroyed++;
                        dashBar.SetDashEnergy(currentObstaclesDestroyed); 

                        if (currentObstaclesDestroyed >= requiredObstaclesDestroyed)
                        {
                            dashBar.gameObject.GetComponent<DashBar>().enableDashButton();
                        }

                        if (currentObstaclesDestroyed >= 20)
                        {
                            bossSpawner.GetComponent<BossSpawner>().spawnBoss();
                        }

                        score += 5;

                        if (!bossSpawned && bossEnemy == null)
                        {
                            bossEnemy = null;
                        }

                        else
                        {
                            Debug.Log("Remove Enemy");
                            bossEnemy.gameObject.GetComponent<BossEnemy>().removeEnemy();
                            return;
                        }


                        return;
                    }

                    else if (targetEnemy.tag != "LeftSwipe" && Distance.x < -swipeRange)
                    {
                        Debug.Log("LeftSwipe Not Match");
                        damagePlayer();
                        return;
                    }

                    if (targetEnemy.tag == "RightSwipe" && Distance.x > swipeRange)
                    {

                        Debug.Log("RightSwipe Match");
                        this.transform.position = new Vector2(transform.position.x, transform.position.y + 1);
                        collider.gameObject.GetComponent<Enemy>().DestroyItself();
                        targetEnemy = null;
                        playerInput = "RightSwipe";
                        outputText.text = playerInput;
                        stopTouch = true;
                        currentObstaclesDestroyed++;
                        dashBar.SetDashEnergy(currentObstaclesDestroyed);

                        if (currentObstaclesDestroyed >= requiredObstaclesDestroyed)
                        {
                            dashBar.gameObject.GetComponent<DashBar>().enableDashButton();
                        }

                        if (currentObstaclesDestroyed >= 20)
                        {
                            bossSpawner.GetComponent<BossSpawner>().spawnBoss();
                        }

                        if (!bossSpawned && bossEnemy == null)
                        {
                            bossEnemy = null;
                        }

                        else
                        {
                            Debug.Log("Remove Enemy");
                            bossEnemy.gameObject.GetComponent<BossEnemy>().removeEnemy();
                            return;
                        }

                        score += 5;
                        return;
                    }

                    else if (targetEnemy.tag != "RightSwipe" && Distance.x > swipeRange)
                    {
                        Debug.Log("RightSwipe Not Match");
                        damagePlayer();
                        return;
                    }

                    if (targetEnemy.tag == "UpSwipe" && Distance.y > swipeRange)
                    {
                        Debug.Log("UpSwipe Match");
                        this.transform.position = new Vector2(transform.position.x, transform.position.y + 1);
                        collider.gameObject.GetComponent<Enemy>().DestroyItself();
                        targetEnemy = null;
                        playerInput = "UpSwipe";
                        outputText.text = playerInput;
                        stopTouch = true;
                        currentObstaclesDestroyed++;
                        dashBar.SetDashEnergy(currentObstaclesDestroyed);

                        if (currentObstaclesDestroyed >= requiredObstaclesDestroyed)
                        {
                            dashBar.gameObject.GetComponent<DashBar>().enableDashButton();
                        }

                        if (currentObstaclesDestroyed >= 20)
                        {
                            bossSpawner.GetComponent<BossSpawner>().spawnBoss();
                        }

                        if (!bossSpawned && bossEnemy == null)
                        {
                            bossEnemy = null;
                        }

                        else
                        {
                            Debug.Log("Remove Enemy");
                            bossEnemy.gameObject.GetComponent<BossEnemy>().removeEnemy();
                            return;
                        }
                        score += 5;
                        return;
                    }

                    else if (targetEnemy.tag != "UpSwipe" && Distance.y > swipeRange)
                    {
                        Debug.Log("UpSwipe Not Match");
                        damagePlayer();
                        return;
                    }

                    if (targetEnemy.tag == "DownSwipe" && Distance.y < -swipeRange)
                    {

                        Debug.Log("DownSwipe Match");
                        this.transform.position = new Vector2(transform.position.x, transform.position.y + 1);
                        collider.gameObject.GetComponent<Enemy>().DestroyItself();
                        targetEnemy = null;
                        playerInput = "DownSwipe";
                        outputText.text = playerInput;
                        stopTouch = true;
                        currentObstaclesDestroyed++;
                        dashBar.SetDashEnergy(currentObstaclesDestroyed);


                        if (currentObstaclesDestroyed >= requiredObstaclesDestroyed)
                        {
                            EnableDashButton();
                        }

                        if (currentObstaclesDestroyed >= 20)
                        {
                            bossSpawner.GetComponent<BossSpawner>().spawnBoss();
                        }

                        if (!bossSpawned && bossEnemy == null)
                        {
                            bossEnemy = null;
                        }

                        else
                        {
                            Debug.Log("Remove Enemy");
                            bossEnemy.gameObject.GetComponent<BossEnemy>().removeEnemy();
                            return;
                        }

                        score += 5;
                        return;
                    }

                    else if (targetEnemy.tag != "DownSwipe" && Distance.y < -swipeRange)
                    {
                        Debug.Log("DownSwipe Not Match");
                        damagePlayer();
                        return;

                    }
                }

            }

            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                stopTouch = false;

                endTouchPosition = Input.GetTouch(0).position;

                Vector2 Distance = endTouchPosition - startTouchPosition;

            }
        }
    }

    public void EnableDashButton()
    {
        dashBar.gameObject.GetComponent<DashBar>().enableDashButton();
    }

    public void DisableDashButton()
    {
        dashBar.gameObject.GetComponent<DashBar>().disableDashButton();
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject == targetEnemy)
        {
            targetEnemy = null;
            playerInput = null;
        }
    }

    void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.GetComponent<Enemy>())
        {
            collision.gameObject.GetComponent<Enemy>().DestroyItself();
            damagePlayer();
        }
    }

    void damagePlayer()
    {
        if (life <= 0) return;

        life--;

        if (life <= 0)
        {
            transform.GetChild(2).parent = null;
            transform.GetChild(1).parent = null;
            transform.GetChild(0).parent = null;
            GameObject gameManager = GameObject.Find("GameManager");
            gameManager.GetComponent<GameManager>().PlayerAlive();

        }
    }

    public void ActivatePowerUp()
    {
        Debug.Log("Dashing");
        dashButton.gameObject.SetActive(false);
        currentObstaclesDestroyed = 0;
        dashBar.SetDashEnergy(currentObstaclesDestroyed);

        //Remove all child of this gameobject
        Transform mainCamera = gameObject.transform.Find("Main Camera");
        Transform tileSpawner = gameObject.transform.Find("TileSpawner");
        Transform enemySpawner = gameObject.transform.Find("EnemySpawner");
        Transform pickupSpawner = gameObject.transform.Find("PickupSpawner");

        mainCamera.parent = null;
        tileSpawner.parent = null;
        pickupSpawner.parent = null;
        enemySpawner.parent = null;


        //Move this object at the middle
        this.transform.position = new Vector2(transform.position.x - 2, transform.position.y + 2);

        //Re attach all children of the gameobject
        GameObject childMainCamera = GameObject.Find("Main Camera");
        childMainCamera.transform.parent = this.gameObject.transform;
        GameObject childTileSpawner = GameObject.Find("TileSpawner");
        childTileSpawner.transform.parent = this.gameObject.transform;
        GameObject childEnemySpawner = GameObject.Find("EnemySpawner");
        childEnemySpawner.transform.parent = this.gameObject.transform;
        GameObject childPickupSpawner = GameObject.Find("PickupSpawner");
        childPickupSpawner.transform.parent = this.gameObject.transform;

        //Multiply spawner multiplier by 0.5
        childEnemySpawner.gameObject.GetComponent<EnemySpawner>().multiplier = 0.5f;
        childTileSpawner.gameObject.GetComponent<TileSpawner>().multiplier = 0.3f;

        powerUpActivated = true;
    }
    private void PowerUpActive()
    {
        StartCoroutine("Dashing");
    }

    private IEnumerator Dashing()
    {

        this.transform.position = new Vector2(transform.position.x, transform.position.y + 25 * Time.deltaTime);

        yield return new WaitForSeconds(5);

        //Remove all child of this gameobject
        Transform mainCamera = gameObject.transform.Find("Main Camera");
        Transform tileSpawner = gameObject.transform.Find("TileSpawner");
        Transform enemySpawner = gameObject.transform.Find("EnemySpawner");
        Transform pickupSpawner = gameObject.transform.Find("PickupSpawner");

        mainCamera.parent = null;
        enemySpawner.parent = null;
        tileSpawner.parent = null;
        pickupSpawner.parent = null;

        //Move this object back to its original position
        this.transform.position = new Vector2(transform.position.x + 2, transform.position.y - 2);

        //Re attach all children of the gameobject
        GameObject childMainCamera = GameObject.Find("Main Camera");
        childMainCamera.transform.parent = this.gameObject.transform;
        GameObject childTileSpawner = GameObject.Find("TileSpawner");
        childTileSpawner.transform.parent = this.gameObject.transform;
        GameObject childPickupSpawner = GameObject.Find("PickupSpawner");
        childPickupSpawner.transform.parent = this.gameObject.transform;


        //Multiply spawner multiplier by 0.5
        GameObject childEnemySpawner = GameObject.Find("EnemySpawner");
        childEnemySpawner.transform.parent = this.gameObject.transform;
        childEnemySpawner.gameObject.GetComponent<EnemySpawner>().multiplier = 1;
        childTileSpawner.gameObject.GetComponent<TileSpawner>().multiplier = 1;
        
        powerUpActivated = false;
        Debug.Log("Stop dashing now");
        StopCoroutine("Dashing");
    }
}
