﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawner : MonoBehaviour
{
    public int bossKilled = 0;
    public GameObject player;
    public GameObject bossEnemy;
    public GameObject enemySpawner;

    void Awake()
    {
        player = GameObject.Find("Player");
        enemySpawner = GameObject.Find("EnemySpawner");
    }

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void spawnBoss()
    {
        player.GetComponent<PlayerScript>().DisableDashButton();
        enemySpawner.GetComponent<EnemySpawner>().bossSpawn = true;
        StartCoroutine("SpawnBoss");
    }

    private IEnumerator SpawnBoss()
    {
        yield return new WaitForSeconds(3);
        if (player.GetComponent<PlayerScript>().currentObstaclesDestroyed >= 20)
        {
            player.GetComponent<PlayerScript>().currentObstaclesDestroyed = 0;
            player.GetComponent<PlayerScript>().BossSpawned();
            if (player.GetComponent<PlayerScript>().currentObstaclesDestroyed >= player.GetComponent<PlayerScript>().requiredObstaclesDestroyed)
            {
                player.GetComponent<PlayerScript>().EnableDashButton();
            }
            
            Instantiate(bossEnemy, transform.position, transform.rotation);
            bossEnemy.GetComponent<BossEnemy>().bossHealth += bossKilled;

        }
    }

    public void bossDeath()
    {
        enemySpawner.GetComponent<EnemySpawner>().bossSpawn = false;
        player.GetComponent<PlayerScript>().BossDead();
        enemySpawner.GetComponent<EnemySpawner>().Spawn();
        bossKilled += 1;

    }

}
