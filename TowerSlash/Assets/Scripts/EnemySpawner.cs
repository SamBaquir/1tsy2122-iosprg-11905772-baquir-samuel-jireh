﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject[] enemies;
    public GameObject player;

    public bool bossSpawn = false;

    public float multiplier = 1;
    private float delayTimer = 0;
    private bool delayOver = false;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (!bossSpawn)
        {
            SpawnerLogic();
        }  
    }

    private void SpawnerLogic()
    {
        if (!delayOver)
        {
            delayTimer++;
        }
        if (delayTimer == 15)
        {
            Spawn();
            delayOver = true;
            delayTimer = 16;
        }
    }

    public void Spawn()
    {
        StartCoroutine("SpawnEnemy");

    }

    private IEnumerator SpawnEnemy()
    {
        /*
        if (!player.GetComponent<PlayerScript>().powerUpActivated)
        {
          this.transform.position = new Vector2(transform.position.x, transform.position.y + 4);
        }
        */

        Destroy(Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Length)], transform.position, transform.rotation), 5);

        yield return new WaitForSeconds(Random.Range(0.8f, 1.2f) * multiplier);
        if (!bossSpawn)
        {
            StartCoroutine("SpawnEnemy");
        }
        
    }

}
