﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSpawner : MonoBehaviour
{
    public float multiplier = 1;
    public GameObject[] tiles;
    private float delayTimer = 0;
    private bool delayOver = false;
    
    // Start is called before the first frame update
    void Start()
    {
        SpawnerLogic();
    }

    // Update is called once per frame
    void Update()
    {
        SpawnerLogic();
    }

    private void SpawnerLogic()
    {
        if (!delayOver)
        {
            delayTimer++;
        }
        if (delayTimer == 5)
        {
            Spawn();
            delayOver = true;
            delayTimer = 6;
        }
    }

    private void Spawn()
    {
        StartCoroutine("SpawnTile");

    }
    
    private IEnumerator SpawnTile()
    {
        Destroy(Instantiate(tiles[UnityEngine.Random.Range(0, tiles.Length)], transform.position, transform.rotation), 2);

        yield return new WaitForSeconds(0.3f * multiplier);
        StartCoroutine("SpawnTile");
    }

}
