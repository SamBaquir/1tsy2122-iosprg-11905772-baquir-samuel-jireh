﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject gameOverUI;
    public GameObject gameUI;

    public GameObject player;
    public bool playerDead;

    public Text scoreText;
    public Text lifeText;

    void Awake()
    {
        gameUI = GameObject.Find("GameUI");
    }

    // Start is called before the first frame update
    void Start()
    {
        playerDead = false;
    }

    // Update is called once per frame
    void Update()
    {
        lifeText.text = "Life: " + player.gameObject.GetComponent<PlayerScript>().life;
        scoreText.text = "Score: " + player.gameObject.GetComponent<PlayerScript>().score;
    }

    public void PlayerAlive()
    {
        Debug.Log("Pausegame");
        GameObject player = GameObject.Find("Player");
        if (player.GetComponent<PlayerScript>().life <= 0)
        {
            gameUI.gameObject.SetActive(false);
            Time.timeScale = 0;
            player.gameObject.SetActive(false);
            gameOverUI.gameObject.SetActive(true);
        };
    }

    public void RestartGame()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
        Time.timeScale = 1;

    }
}
