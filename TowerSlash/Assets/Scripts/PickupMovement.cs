﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupMovement : MonoBehaviour
{
   
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate()
    {
        transform.position = new Vector2(transform.position.x, transform.position.y - 0.02f);
    }

    public void DestroySelf()
    {
        Destroy(this.gameObject);
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.GetComponent<PlayerScript>())
        {
            collider.gameObject.GetComponent<PlayerScript>().life++;
            Destroy(this.gameObject);
        }

    }
}
