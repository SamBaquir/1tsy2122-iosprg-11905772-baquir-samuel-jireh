﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingDirectionEnemy : Enemy
{
    GameObject arrow;
    bool playerNear = false;

    void Awake()
    {
        assignedDirection = swipeDirections[Random.Range(0, swipeDirections.Length)];
        AssignDirection();
        arrow = transform.GetChild(0).gameObject;
    }

    // Start is called before the first frame update
    void Start()
    {
        GetRigidBody();
    }

    // Update is called once per frame
    void Update()
    {
        Movement();

        if (this.transform.position.y <= -11.53)
        {
            Destroy(this.gameObject);
        }

        
        if (!playerNear)
        {
            arrow.transform.Rotate(transform.rotation.x, transform.rotation.y, 90 * Time.deltaTime * 16);
        }
        
    }

    public override void ExposeItself()
    {
        playerNear = true;
        base.ExposeItself();
        if (assignedDirection == "Left")
        {
            arrow.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        }
        else if (assignedDirection == "Down")
        {
            arrow.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 90));
        }
        else if (assignedDirection == "Right")
        {
            arrow.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 180));
        }
        else if (assignedDirection == "Up")
        {
            arrow.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 270));
        }
        spriteRenderer.color = Color.green;
    }
}
