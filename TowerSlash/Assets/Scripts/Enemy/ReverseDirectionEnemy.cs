﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReverseDirectionEnemy : Enemy
{
    Color newRedColor = new Color(1f, 0f, 0f, 0.6f);

    void Awake()
    {
        assignedDirection = swipeDirections[Random.Range(0, swipeDirections.Length)];
        AssignDirection();
        GameObject arrow = transform.GetChild(0).gameObject;
        spriteRenderer = arrow.GetComponent<SpriteRenderer>();
        spriteRenderer.color = newRedColor;

        displayReverseDirection();
    }

    void displayReverseDirection()
    {
        if (assignedDirection == "Left")
        {
            GameObject arrow = transform.GetChild(0).gameObject;
            arrow.transform.Rotate(new Vector3(0, 0, 180), Space.Self);
        }
        else if (assignedDirection == "Down")
        {
            GameObject arrow = transform.GetChild(0).gameObject;
            arrow.transform.Rotate(new Vector3(0, 0, 270), Space.Self);
        }
        else if (assignedDirection == "Up")
        {
            GameObject arrow = transform.GetChild(0).gameObject;
            arrow.transform.Rotate(new Vector3(0, 0, 90), Space.Self);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        GetRigidBody();
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.y <= -11.53)
        {
            Destroy(this.gameObject);
        }
        Movement();
    }

    public override void ExposeItself()
    {
        base.ExposeItself();
        spriteRenderer.color = Color.red;
    }
}
