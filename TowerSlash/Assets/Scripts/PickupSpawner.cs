﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawner : MonoBehaviour
{
    public GameObject pickup;
    private float delayTimer = 0;
    private bool delayOver = false;


    // Start is called before the first frame update
    void Start()
    {
        SpawnerLogic();
    }

    // Update is called once per frame
    void Update()
    {
        SpawnerLogic();
    }

    private void SpawnerLogic()
    {
        if (!delayOver)
        {
            delayTimer++;
        }
        if (delayTimer == Random.Range(300, 360))
        {
            Spawn();
            delayOver = true;
            delayTimer = 6001;
        }
    }

    private void Spawn()
    {
        StartCoroutine("SpawnPickup");

    }

    private IEnumerator SpawnPickup()
    {
        Instantiate(pickup, transform.position, transform.rotation);

        yield return new WaitForSeconds(Random.Range(60, 120));
        StartCoroutine("SpawnPickup");
    }
}
