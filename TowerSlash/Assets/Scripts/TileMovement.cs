﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMovement : MonoBehaviour
{
    private float moveSpeed = 15f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.y <= -11.53)
        {
            Destroy(this.gameObject);
        }
        transform.Translate(new Vector2(0f, -1f) * moveSpeed * Time.deltaTime);
    }
}
