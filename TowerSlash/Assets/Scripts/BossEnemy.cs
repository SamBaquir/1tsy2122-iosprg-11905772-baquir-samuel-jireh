﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossEnemy : Enemy
{
    public GameObject[] setOfDirections;
    //public GameObject bossEnemy;

    public List<Enemy> enemyToSpawn = new List<Enemy>();
    public int bossHealth = 0;
    private float delayTimer = 0;
    private bool delayOver = false;
    public GameObject player;
    [SerializeField] private int counter = 0;

    // Start is called before the first frame update

    void Awake()
    {
        player = GameObject.Find("Player");
        player.GetComponent<PlayerScript>().BossSpawned();
    }

    void Start()
    {

        /*
        bossEnemy = GameObject.Find("BossSpawner");
        this.transform.parent = bossEnemy.transform;
        */
        /*
        player = GameObject.Find("Player");
        player.GetComponent<PlayerScript>().BossSpawned();
        */
    }

    // Update is called once per frame
    void Update()
    {
        SpawnerLogic();
        moveUp();


    }

    public void playerDashing()
    {
        GameObject player = GameObject.Find("Player");
        this.transform.position = new Vector2(transform.position.x, transform.position.y + 25 * Time.deltaTime);
        /*
        if (player.GetComponent<PlayerScript>().powerUpActivated)
        {
            StartCoroutine("TakeDamageFromDash");
        }
        */
    }

    private IEnumerator TakeDamageFromDash()
    {
        Debug.Log("Start Taking Damage");
        yield return new WaitForSeconds(1);
        bossHealth--;
        if (bossHealth <= 0)
        {
            Die();
            StopCoroutine("TakeDamageFromDash");
            yield break;
        }
        else
        {
            StartCoroutine("TakeDamageFromDash");
        }
    }

    private void SpawnerLogic()
    {
        if (!delayOver)
        {
            delayTimer++;
        }
        if (delayTimer == 15)
        {
            Spawn();
            delayOver = true;
            delayTimer = 46;
        }
    }

    public void moveUp()
    {
        if (player.GetComponent<PlayerScript>().dashed == true)
        {
            this.transform.position = new Vector2(transform.position.x, transform.position.y + 1);
        }
    }

    private void Spawn()
    {
        StartCoroutine("SpawnEnemy");

    }

    public void removeEnemy()
    {
        this.transform.position = new Vector2(transform.position.x, transform.position.y + 1);
        enemyToSpawn.RemoveAt(0);
        if (enemyToSpawn.Count <= 0)
        {
            TakeDamage();
        }
    }

    private IEnumerator SpawnEnemy()
    {
        GameObject newEnemy = (GameObject)Instantiate(setOfDirections[UnityEngine.Random.Range(0, setOfDirections.Length)], transform.position, transform.rotation);
        enemyToSpawn.Add(newEnemy.GetComponent<Enemy>());
        counter++;
        yield return new WaitForSeconds(Random.Range(0.5f, 0.8f));
        if (counter < 3)
        {
            StartCoroutine("SpawnEnemy");
        }
        
    }

    public void TakeDamage()
    {
        if (enemyToSpawn.Count <= 0)
        {
            counter = 0;
            bossHealth--;
            if (bossHealth <= 0)
            {
                Die();
            }
            else
            {
                StartCoroutine("SpawnEnemy");
            }
        }
    }

    public void Die()
    {
        if (bossHealth <= 0)
        {
            GameObject bossSpawner = GameObject.Find("BossSpawner");
            bossSpawner.GetComponent<BossSpawner>().bossDeath();
            Destroy(this.gameObject);
        }

    }
}
